function performClick(elemId) {
  var elem = document.getElementById(elemId);
  if (elem && document.createEvent) {
    var evt = document.createEvent("MouseEvents");
    evt.initEvent("click", true, false);
    elem.dispatchEvent(evt);
  }
}

var imagenes = [];

function showImage(src, target) {
  var fr = new FileReader();
  // when image is loaded, set the src of the image where you want to display it
  fr.onload = function (e) { target.setAttribute("src", this.result); };
  fr.readAsDataURL(src.files[0]);
}

function addImg() {

  if (imagenes.length >= 3) {
    window.alert("El maximo de imagenes son 3");
    return null;
  }
  var fr = new FileReader();
  var img = document.getElementById("inputImg").files[0];
  var zonaImagenes = document.getElementById("imgs");
  imagenes.push(img);
  console.log(imagenes.length);
  var imgElement = document.createElement("img");
  showImage(document.getElementById("inputImg"), imgElement);
  imgElement.classList.add("img-fluid");
  var rmvButton = document.createElement("button");
  rmvButton.setAttribute("type", "button");
  rmvButton.classList.add("btn");
  rmvButton.classList.add("btn-outline-danger");
  rmvButton.innerHTML = "X";
  var container = document.createElement("div");
  container.appendChild(rmvButton);
  container.appendChild(document.createElement("br"));
  container.appendChild(imgElement);
  container.setAttribute("style", "margin-bottom: 10px");
  zonaImagenes.appendChild(container);
  rmvButton.onclick = function () {
    zonaImagenes.removeChild(container);
    const index = imagenes.indexOf(img);
    if (index > -1) {
      imagenes.splice(index, 1);
    }
  }
}
